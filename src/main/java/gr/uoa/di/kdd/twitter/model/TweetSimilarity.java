package gr.uoa.di.kdd.twitter.model;

public class TweetSimilarity {
	
	/**
	 * @uml.property  name="text"
	 */
	private String text;
	
	/**
	 * @uml.property  name="similarity"
	 */
	private double similarity;

	/**
	 * @return
	 * @uml.property  name="text"
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text
	 * @uml.property  name="text"
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return
	 * @uml.property  name="similarity"
	 */
	public double getSimilarity() {
		return similarity;
	}

	/**
	 * @param similarity
	 * @uml.property  name="similarity"
	 */
	public void setSimilarity(double similarity) {
		this.similarity = similarity;
	}

	@Override
	public String toString() {
		return "TweetSimilarity [text=" + text + ", similarity=" + similarity
				+ "]";
	}
}
