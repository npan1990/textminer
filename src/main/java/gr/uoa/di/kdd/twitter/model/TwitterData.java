package gr.uoa.di.kdd.twitter.model;
import java.io.Serializable;
import java.util.Date;

public class TwitterData implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * @uml.property  name="text"
	 */
	private String text;
	/**
	 * @uml.property  name="user"
	 */
	private String user;
	/**
	 * @uml.property  name="created_at"
	 */
	private String created_at;
	/**
	 * @uml.property  name="date"
	 */
	private Date date;
	/**
	 * @uml.property  name="timestamp"
	 */
	private long timestamp;
	/**
	 * @uml.property  name="location"
	 */
	private String location;
	/**
	 * @uml.property  name="fingerprint"
	 */
	private int fingerprint = -1;
	
	public TwitterData(String text, String user, String created_at, Date date,
			long timestamp, String location) {
		this.text = text;
		this.user = user;
		this.created_at = created_at;
		this.date = date;
		this.timestamp = timestamp;
		this.location = location;
	}
	
	public TwitterData() {
	}
	
	/**
	 * @return
	 * @uml.property  name="location"
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location
	 * @uml.property  name="location"
	 */
	public void setLocation(String location) {
		this.location = location;
	}


	
	
	/**
	 * @return
	 * @uml.property  name="text"
	 */
	public String getText() {
		return text;
	}



	/**
	 * @param text
	 * @uml.property  name="text"
	 */
	public void setText(String text) {
		this.text = text;
	}



	/**
	 * @return
	 * @uml.property  name="user"
	 */
	public String getUser() {
		return user;
	}



	/**
	 * @param user
	 * @uml.property  name="user"
	 */
	public void setUser(String user) {
		this.user = user;
	}



	/**
	 * @return
	 * @uml.property  name="created_at"
	 */
	public String getCreated_at() {
		return created_at;
	}



	/**
	 * @param created_at
	 * @uml.property  name="created_at"
	 */
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}



	/**
	 * @return
	 * @uml.property  name="date"
	 */
	public Date getDate() {
		return date;
	}



	/**
	 * @param date
	 * @uml.property  name="date"
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	
	/**
	 * @return
	 * @uml.property  name="timestamp"
	 */
	public long getTimestamp() {
		return timestamp;
	}


	/**
	 * @param timestamp
	 * @uml.property  name="timestamp"
	 */
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return
	 * @uml.property  name="fingerprint"
	 */
	public int getFingerprint() {
		if (this.fingerprint == -1) {
			this.fingerprint = this.text.hashCode();
		}
 		
		return this.fingerprint;
	}

	/**
	 * @param fingerprint
	 * @uml.property  name="fingerprint"
	 */
	public void setFingerprint(int fingerprint) {
		this.fingerprint = fingerprint;
	}

	@Override
	public String toString() {
		return "TwitterData text=" + text + ", user=" + user + ", created_at="
				+ created_at + ", date=" + date + ", timestamp=" + timestamp;
	}

	public int getHashValue() {
		return this.getFingerprint();
	}
}
