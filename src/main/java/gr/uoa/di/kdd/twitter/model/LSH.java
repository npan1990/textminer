package gr.uoa.di.kdd.twitter.model;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;


public class LSH {
	/**
	 * @uml.property  name="families"
	 * @uml.associationEnd  multiplicity="(0 -1)" elementType="gr.uoa.di.kdd.twitter.model.CosineFamily"
	 */
	List<CosineFamily> families;
	/**
	 * @uml.property  name="l"
	 */
	public int L;
	/**
	 * @uml.property  name="k"
	 */
	public int K;
	/**
	 * @uml.property  name="d"
	 */
	public int D;
	public LSH(int L,int K,int D)
	{
		families=new ArrayList<CosineFamily>();
		long seed = 10000;
		for (int i=0;i<L;i++)
		{
			families.add(new CosineFamily(K,D, seed));
			seed += 1000;
		}
		this.L=L;
		this.K=K;
		this.D=D;
	}
	public List<Integer> getFingerprint(List<Integer> document)
	{
		List<Integer> fingerprints=new ArrayList<Integer>();
		for (int i=0;i<L;i++)
		{
			fingerprints.add(families.get(i).getFingerprint(document));
		}
		return fingerprints;
	}
	public int getBucketSpace()
	{
		return 1<<K;
	}
	public static void main(String[] args) throws IOException
	{
		TwitterVectors twitterVectors=new TwitterVectors(1);
		System.out.println(twitterVectors.getDimension());
		LSH lsh=new LSH(1,3,twitterVectors.getDimension());
	
		HashMap<Integer,List<String>> buckets = new HashMap<Integer,List<String>>();
		for (int i=0;i<lsh.getBucketSpace();i++)
		{
			buckets.put(i, new ArrayList<String>());
		}
		/*
		tweets.add("Justin bieber concert on March");
		tweets.add("Justin bieber I love you");
		tweets.add("Justin bieber playing tenis");
		tweets.add("my name is justin");
		tweets.add("Eurogroup meeting on Greece");
		tweets.add("Greece president Tsipras and Yiannis Varoufakis");
		tweets.add("the Dept of Greece is highly increased");
		tweets.add("Holidays in athens this summer");
		tweets.add("I have been to americ during summer");
		tweets.add("What About Greek Islands?");
		for (String t:tweets)
		{
			System.out.println(t+":"+lsh.getFingerprint(twitterVectors.toVector(t)));
		}
		*/
		String input;
		BufferedReader bufferedReader = new BufferedReader(new FileReader("bieber_tweets.json"));
		while((input=bufferedReader.readLine())!=null)
		{
			try
			{
				JSONObject jsonObject=new JSONObject(input);
				String text=(String) jsonObject.getString("text");
				List<Integer> fingerprint=lsh.getFingerprint(twitterVectors.toVector(text));
				for (int i:fingerprint)
				buckets.get(fingerprint.get(i)).add(text);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		bufferedReader.close();
		
		for (int i=0;i<lsh.getBucketSpace();i++)
		{
			System.out.println("Bucket:"+i+" "+buckets.get(i).size());
		}
		/*
		 * Print Buckets 0
		 */
		System.out.println("Printing Bucket 0");
		int count=0;
		for (String tweet:buckets.get(0))
		{
			System.out.println(tweet);
			if (count>10)
				break;
			count++;
		}
		/*
		 * Print Buckets 1
		 */
		System.out.println("Printing Bucket 1");
		count=0;
		for (String tweet:buckets.get(1))
		{
			System.out.println(tweet);
			if(count>10)
				break;
			count++;
		}
	}

}
