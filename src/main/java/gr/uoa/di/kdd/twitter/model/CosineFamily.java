package gr.uoa.di.kdd.twitter.model;

import java.io.FileNotFoundException;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CosineFamily {
	/**
	 * @uml.property  name="functions"
	 * @uml.associationEnd  multiplicity="(0 -1)" elementType="gr.uoa.di.kdd.twitter.model.CosineFunction"
	 */
	public List<CosineFunction> functions;
	/**
	 * @uml.property  name="k"
	 */
	int K;
	/**
	 * @uml.property  name="d"
	 */
	int D;
	
	
	public CosineFamily(int K,int D, long startSeed)
	{
		this.K=K;
		this.D=D;
		functions=new ArrayList<CosineFunction>();
		long seed = startSeed;
		for (int i=0;i<K;i++)
		{
			functions.add(new CosineFunction(D,seed));
			seed += 10000;
		}
	}
	public int getFingerprint(List<Integer> document)
	{
		int fingerprint=0;
		int i=0;
		for(CosineFunction f:functions)
		{
			fingerprint+=f.getFingerprint(document)<<i;
			i++;
		}
		return fingerprint;
	}
	public static void main(String[] args) throws FileNotFoundException
	{
		TwitterVectors twitterVectors=new TwitterVectors(2);
		System.out.println(twitterVectors.getDimension());
		CosineFamily lsh=new CosineFamily(2,twitterVectors.getDimension(), 10000);
		List<String> tweets=new LinkedList<String>();
		tweets.add("Justin bieber concert on March");
		tweets.add("Justin bieber I love you");
		tweets.add("Justin bieber playing tenis");
		tweets.add("my name is justin");
		tweets.add("Eurogroup meeting on Greece");
		tweets.add("Greece president Tsipras and Yiannis Varoufakis");
		tweets.add("the Dept of Greece is highly increased");
		tweets.add("Holidays in athens this summer");
		tweets.add("I have been to americ during summer");
		tweets.add("What About Greek Islands?");
		
		
		
		
		for (String t:tweets)
		{
			System.out.println(t+":"+lsh.getFingerprint(twitterVectors.toVector(t)));
		}
	}

}
