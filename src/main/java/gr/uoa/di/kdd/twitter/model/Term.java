package gr.uoa.di.kdd.twitter.model;

public class Term {
	
	
	/**
	 * @uml.property  name="text"
	 */
	public String text;
	/**
	 * @uml.property  name="index"
	 */
	public int index;
	
	
	
	
	
	
	public Term(String text, int index) {
		super();
		this.text = text;
		this.index = index;
	}
	/**
	 * @return
	 * @uml.property  name="text"
	 */
	public String getText() {
		return text;
	}
	/**
	 * @param text
	 * @uml.property  name="text"
	 */
	public void setText(String text) {
		this.text = text;
	}
	/**
	 * @return
	 * @uml.property  name="index"
	 */
	public int getIndex() {
		return index;
	}
	/**
	 * @param index
	 * @uml.property  name="index"
	 */
	public void setIndex(int index) {
		this.index = index;
	}
	
	
	

}
