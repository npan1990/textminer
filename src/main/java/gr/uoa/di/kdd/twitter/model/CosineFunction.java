package gr.uoa.di.kdd.twitter.model;

import java.util.ArrayList;

import java.util.List;
import java.util.Random;

public class CosineFunction {
	
	/**
	 * @uml.property  name="vector"
	 * @uml.associationEnd  multiplicity="(0 -1)" elementType="java.lang.Float"
	 */
	public List<Float> vector;
	/**
	 * @uml.property  name="rn"
	 */
	Random rn;
	/**
	 * @uml.property  name="size"
	 */
	int size;
	
	
	public CosineFunction(int size, long seed)
	{
		System.out.println("Generating Hash Function");
		this.size=size;
		vector=new ArrayList<Float>(size);
		rn=new Random(seed);
		for (int i=0;i<size;i++)
		{
			vector.add(0,(float) rn.nextGaussian());
		}
		System.out.println("Complete");
		
	}
	
	public float getProjection(List<Integer> document)
	{
		float sum=0;
		for(Integer d:document)
		{
			sum+=1.0*vector.get(d);
			//System.out.println(vector.get(d));
		}
		return sum;
	}
	public int getFingerprint(List<Integer> document)
	{
		if (getProjection(document)>0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	

}
