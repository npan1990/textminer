package gr.uoa.di.kdd.twitter.model;

import java.io.Serializable;



/**
 * The class that will be used for keeping detected 
 * events from incoming tweets.
 * @author Nikos Zacheilas
 *
 */

public class DetectedEvent implements Serializable {

	
	/**
	 * @uml.property  name="id"
	 */
	/**
	 * An automatically generated id for the
	 * storage of incoming tuples.
	 */
	protected int id;
	
	/**
	 * The default serial version id.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The text of the event.
	 * @uml.property  name="text"
	 */
	private String text;
	
	/**
	 * The event topic.
	 * @uml.property  name="topic"
	 */
	private String topic;

	/**
	 * The constructor that will initialize the tweet's 
	 * text and the topic.
	 * @param text the tweet's text.
	 * @param topic the tweet's topic.
	 */
	public DetectedEvent(String text, String topic) {
		this.text = text;
		this.topic = topic;
	}

	/**
	 * The method that returns the id.
	 * @return  the id.
	 * @uml.property  name="id"
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * The method that sets the id.
	 * @param id  the new value of the id.
	 * @uml.property  name="id"
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * The method that returns the tweet's text.
	 * @return  the tweet's text.
	 * @uml.property  name="text"
	 */
	public String getText() {
		return this.text;
	}

	/**
	 * The method that will set the tweet's text.
	 * @param text  the tweet's text.
	 * @uml.property  name="text"
	 */
	public void setText(String text) {
		this.text = text;
	}
	
	/**
	 * The method that will return the tweet's topic.
	 * @return  the tweet's topic.
	 * @uml.property  name="topic"
	 */
	public String getTopic() {
		return this.topic;
	}
	
	/**
	 * The method that will set the tweet's  topic.
	 * @param topic  the tweet's topic.
	 * @uml.property  name="topic"
	 */
	public void setTopic(String topic) {
		this.topic = topic;
	}

	@Override
	public String toString() {
		return "DetectedEvent text=" +this.text+ ", topic=" +this.topic;
	}
}