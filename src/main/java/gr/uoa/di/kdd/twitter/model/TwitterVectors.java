package gr.uoa.di.kdd.twitter.model;

import java.io.BufferedReader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONObject;

public class TwitterVectors {
	/**
	 * @uml.property  name="terms"
	 * @uml.associationEnd  multiplicity="(0 -1)" ordering="true" elementType="java.util.List" qualifier="term:java.lang.String java.lang.Integer"
	 */
	public HashMap<String,Integer> terms;
	/**
	 * @uml.property  name="d"
	 */
	private int D;

	public TwitterVectors(int termSize) throws FileNotFoundException
	{
		InputStream in = this.getClass().getClassLoader()
                .getResourceAsStream("terms.csv");
		BufferedReader br=new BufferedReader(new InputStreamReader(in));
		String input;
		int counter=0;
		terms=new HashMap<String,Integer>();
		try {
			while((input=br.readLine())!=null)
			{
				input=input.trim();
				if (input.length()>=termSize)
				{
					new Term(input.toLowerCase(),counter);
					terms.put(input.toLowerCase(), counter);
					counter++;
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		D=counter;
	}
	public List<Integer> toVector(String text)
	{
		text = text.toLowerCase();
		List<Integer> vector=new LinkedList<Integer>();
		String replacedString = text.replaceAll("[^A-Za-z0-9 ]", "");
//		StringUtils.replace(text, "[^A-Za-z0-9 ]", "");
		String[] textSplitted = replacedString.split(" ");
		for (String term : textSplitted)
		{
			if (this.terms.containsKey(term))
			{
				vector.add(this.terms.get(term));
			}
		}
		return vector;
	}
	public int getDimension()
	{
		return D;
	}


	public static void main(String[] args)
	{
		HashMap<String,Integer> terms=new HashMap<String,Integer>();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input;
		/*
		 * 
		 */
		int df=Integer.valueOf(args[0]);
		try {
			int counter=0;
			while ((input=br.readLine())!=null)
			{
				counter++;
				String text=null;
				if (input.contains("created_at"))
				{
					JSONObject tweetJSON;
					try
					{
						tweetJSON=new JSONObject(input);
						text=tweetJSON.getString("text");
						text=text.replaceAll("[^A-Za-z0-9 ]", "");
						for (String term:text.split(" "))
						{
							if (terms.containsKey(term))
							{
								terms.put(term,(terms.get(term)+1));
							}
							else
							{
								terms.put(term, 0);
							}
						}
					}
					catch(Exception e)
					{
						continue;
					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			PrintWriter writer=new PrintWriter("terms.csv");
			for(String term:terms.keySet())
			{
				if(terms.get(term)>df)
				{
					writer.println(term);
					writer.flush();
				}
			}
			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



	}

}
