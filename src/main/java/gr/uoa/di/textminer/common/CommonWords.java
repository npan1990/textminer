package gr.uoa.di.textminer.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CommonWords {
	
	
	HashMap<String,Integer> commonWords;
	int size;
	
	public CommonWords() throws IOException
	{
		InputStream in = getClass().getResourceAsStream("common.txt"); 
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		String line="";
		commonWords=new HashMap<String,Integer>();
		while((line=reader.readLine())!=null)
		{
			line=line.toLowerCase();
			line=line.trim();
			line=Normalizer.normalize(line, Normalizer.Form.NFD);
			line=line.replaceAll("\\p{M}", "");
			commonWords.put(line,1);
		}
	}
	public boolean isCommonWord(String word)
	{
		word=word.toLowerCase();
		word=word.trim();
		word=Normalizer.normalize(word, Normalizer.Form.NFD);
		word=word.replaceAll("\\p{M}", "");
		if (commonWords.containsKey(word))
			return true;
		else
			return false;
	}
	
	
	public HashMap<String, Integer> getCommonWords() {
		return commonWords;
	}
	public void setCommonWords(List<String> commonWords) {
		this.commonWords = new HashMap<String,Integer>();
		for(String word:commonWords)
		{
			word=word.toLowerCase();
			word=word.trim();
			word=Normalizer.normalize(word, Normalizer.Form.NFD);
			word=word.replaceAll("\\p{M}", "");
			this.commonWords.put(word, 1);
		}
		
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public static void main(String[] args) throws IOException
	{
		CommonWords commonWords=new CommonWords();
		System.out.println(commonWords.isCommonWord("γεια"));
	}

}
