package gr.uoa.di.textminer.bit;

public class BitUtils {
	
	
	public static long setBit(long x,int offset)
	{
		return x | (1L << offset);
	}
	public static long unsetBit(long x,int offset)
	{
		return x & ~(1L << offset);
	}
	public static short getBit(long x,int offset)
	{
		return (short) ((x >> offset) & 1L);
	}
	
	
	public static void main(String[] args)
	{
		long x=0;
		for (int i=1;i<=64;i++)
		{
			x=BitUtils.setBit(x,i);
			System.out.println(" Bit "+i+" "+BitUtils.getBit(x, i));
			x=BitUtils.unsetBit(x, i);
			System.out.println(" Bit "+i+" "+BitUtils.getBit(x, i));
		}
	}

}
