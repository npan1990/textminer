package gr.uoa.di.textminer.entities;


import gr.uoa.di.textminer.common.CommonWords;
import gr.uoa.di.textminer.model.TwitterData;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import org.junit.Test;

public class EntityExtractor {
	
	
	
	
	
	
	
	private HashMap<String, String> regexes;
	
	private int MIN_SIZE=3;

	private int N=10;

	//private CommonWords commonWords;
	
	private ArrayList<TwitterData> documents;
	
	
	
	CommonWords commonWords;
	

	public EntityExtractor() throws FileNotFoundException, IOException
	{
		regexes=new HashMap<String,String>();
		regexes.put("((https?|ftp|gopher|telnet|file|Unsure|http):((//)|(\\\\))+[\\w\\d:#@%/;$()~_\\+-=\\\\\\.&]*)", "");
		regexes.put("\\s+", " ");
		regexes.put("[^\\p{L}\\p{N}0-9 ]+", "");
		documents=new ArrayList<TwitterData>();
		commonWords=new CommonWords();
	}
	
	
	public ArrayList<String> extractEntities(TwitterData twitterData)
	{
		ArrayList<String> n=new ArrayList<String>();
		String document=twitterData.getText();
		for (String regex:this.regexes.keySet())
		{
			document=document.replaceAll(regex,this.regexes.get(regex));
		}
		String[] tokens=document.split(" ");
		int g=0;
		for (String t:tokens)
		{
			if(g==0)
			{
				g++;
				continue;
			}
			if (t.length()>MIN_SIZE)
			{
				if (Character.isUpperCase(t.charAt(0)))
						n.add(t);
			}
		}
		return n;
	}
	public ArrayList<String> extractEntities(String document)
	{
		ArrayList<String> n=new ArrayList<String>();
		for (String regex:this.regexes.keySet())
		{
			document=document.replaceAll(regex,this.regexes.get(regex));
		}
		String[] tokens=document.split(" ");
		int g=0;
		for (String t:tokens)
		{
			if(g==0)
			{
				g++;
				continue;
			}
			if (t.length()>MIN_SIZE)
			{
				if (Character.isUpperCase(t.charAt(0)))
						n.add(t);
			}
		}
		return n;
	}
	public ArrayList<String> mostCorrelated(ArrayList<TwitterData> twitterData)
	{	
		HashMap<String,Integer> correlations=new HashMap<String,Integer>();
		for (TwitterData t:twitterData)
		{
			ArrayList<String> entities=this.extractEntities(t);
			if (entities.size()>1)
			{
				for (String e1:entities)
				{
					for(String e2:entities)
					{
						if (commonWords.isCommonWord(e1) || commonWords.isCommonWord(e2))
							continue;
						if(!e1.equals(e2))
						{
							if (correlations.containsKey(e2+"."+e1))
								continue;
							if (!correlations.containsKey(e1+"."+e2))
								correlations.put(e1+"."+e2,0);
							correlations.put(e1+"."+e2,correlations.get(e1+"."+e2)+1);
						}
					}
				}
			}
		}
		correlations=(HashMap<String, Integer>) SortMapOnValueIntegerExample.sortByComparator(correlations);
		ArrayList<String> topN=new ArrayList<String>();
		int n=0;
		for(String e:correlations.keySet())
		{
			topN.add(e);
		}
		Collections.reverse(topN);
		if (topN.size()==0)
			return null;
		return new ArrayList<String>(topN.subList(1, topN.size()>N?N:topN.size()));
	}
	public void insert(TwitterData twitterData)
	{
		documents.add(twitterData);
	}
	public void clear()
	{
		documents=new ArrayList<TwitterData>();
	}
	public ArrayList<String> getMostCorrelated()
	{
		return mostCorrelated(this.documents);
	}
	
	@Test
	public void test() throws FileNotFoundException, IOException
	{
		EntityExtractor e=new EntityExtractor();
	}

}
