package gr.uoa.di.textminer.entities;

import gr.uoa.di.textminer.corpus.NGramExtractor;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class DictionaryEntityExtractor {
	
	
	HashMap<String,Integer> entities;
	
	
	public DictionaryEntityExtractor(String filename) throws FileNotFoundException, IOException
	{
		entities=new HashMap<String,Integer>();
		try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		    	line=line.trim();
		    	line=line.toLowerCase();
		    	entities.put(line,1);
		    }
		}
	}
	public boolean isEntity(String entity)
	{
		entity=entity.toLowerCase();
		if (entities.containsKey(entity))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public ArrayList<String> isEntity(String document,int n)
	{
		ArrayList<String> entitiesFound=new ArrayList<String>();
		//ArrayList<String> candidateEntity=NGramExtractor.extractNgrams(document, n);
		/*for (String e:candidateEntity)
		{
			if (isEntity(e))
				entitiesFound.add(e);
		}*/
		return entitiesFound;
		
	}

}
