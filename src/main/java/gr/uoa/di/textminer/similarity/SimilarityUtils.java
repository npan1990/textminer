package gr.uoa.di.textminer.similarity;

import gr.uoa.di.textminer.hash.SimHasher;

import java.util.HashMap;

public class SimilarityUtils {
	public static double cosineSimilarity(HashMap<Integer,Double> document1,HashMap<Integer,Double> document2)
	{
		if(document1.size()==0 || document2.size()==0)
			return 0;
		double sum=0;
		if (document1.size()<document2.size())
		{
			for (int termInd:document1.keySet())
			{
				if(document2.containsKey(termInd))
				{
					sum+=document1.get(termInd)*document2.get(termInd);
				}
			}
		}
		else
		{
			for (int termInd:document2.keySet())
			{
				if(document1.containsKey(termInd))
				{
					sum+=document1.get(termInd)*document2.get(termInd);
				}
			}
		}
		double denominator1=0,denominator2=0;
		for (int termInd:document1.keySet())
		{
			denominator1+=Math.pow(document1.get(termInd),2);
		}
		for (int termInd:document2.keySet())
		{
			denominator2+=Math.pow(document2.get(termInd),2);
		}
		denominator1=Math.sqrt(denominator1);
		denominator2=Math.sqrt(denominator2);
		double distance=sum/(denominator1*denominator2);
		return distance;
	}
	public static double cosineSimilarityDocument(HashMap<String,Double> document1,HashMap<String,Double> document2)
	{
		if(document1.size()==0 || document2.size()==0)
			return 0;
		double sum=0;
		if (document1.size()<document2.size())
		{
			for (String termInd:document1.keySet())
			{
				if(document2.containsKey(termInd))
				{
					sum+=document1.get(termInd)*document2.get(termInd);
				}
			}
		}
		else
		{

			for (String termInd:document2.keySet())
			{
				if(document1.containsKey(termInd))
				{
					sum+=document1.get(termInd)*document2.get(termInd);
				}
			}
		}
		double denominator1=0,denominator2=0;
		for (String termInd:document1.keySet())
		{
			denominator1+=Math.pow(document1.get(termInd),2);
		}
		for (String termInd:document2.keySet())
		{
			denominator2+=Math.pow(document2.get(termInd),2);
		}
		denominator1=Math.sqrt(denominator1);
		denominator2=Math.sqrt(denominator2);
		double distance=sum/(denominator1*denominator2);
		return distance;
	}
	
	
	
	public static double simhashSimilarity(HashMap<String,Double> document1,HashMap<String,Double> document2,SimHasher similarityHasher)
	{
		long n1=similarityHasher.hash(document1);
		long n2=similarityHasher.hash(document2);
		return similarityHasher.getD()-similarityHasher.distance(n1,n2);
	}

}
