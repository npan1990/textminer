package gr.uoa.di.textminer.sentiment;

import gr.uoa.di.textminer.corpus.NGramExtractor;
import gr.uoa.di.textminer.model.TwitterData;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SentimentAnalyzer {
	
	
	HashMap<String,Integer> positiveKeywords;
	HashMap<String,Integer> negativeKeywords;
	Logger log;
	
	ArrayList<TwitterData> documents;
	
	
	public SentimentAnalyzer() throws IOException
	{
		documents=new ArrayList<TwitterData>();
		positiveKeywords=new HashMap<String,Integer>();
		negativeKeywords=new HashMap<String,Integer>();
		log=LoggerFactory.getLogger(SentimentAnalyzer.class.getName());
		InputStream in = this.getClass().getResourceAsStream("positive.txt");
		BufferedReader input = new BufferedReader(new InputStreamReader(in));
		String line;
		while((line=input.readLine())!=null)
		{
			line=line.toLowerCase();
			line=line.trim();
			line=Normalizer.normalize(line, Normalizer.Form.NFD);
			line=line.replaceAll("\\p{M}", "");
			positiveKeywords.put(line, 1);
		}
		in = this.getClass().getResourceAsStream("negative.txt");
		input = new BufferedReader(new InputStreamReader(in));
		while((line=input.readLine())!=null)
		{
			line=line.toLowerCase();
			line=line.trim();
			line=Normalizer.normalize(line, Normalizer.Form.NFD);
			line=line.replaceAll("\\p{M}", "");
			negativeKeywords.put(line, 1);
		}
		log.info("Sentiment Analyzer Created");
	}
	public double extractSentiment(String document)
	{
		int positivesFound=0;
		int negativesFound=0;
		ArrayList<String> unigrams=NGramExtractor.extractNgrams(document, 1, 0);
		int sentimentKeywords=1;
		for(String k:unigrams)
		{
			k=k.toLowerCase();
			k=k.trim();
			k=Normalizer.normalize(k, Normalizer.Form.NFD);
			k=k.replaceAll("\\p{M}", "");
			if (positiveKeywords.containsKey(k))
			{
				positivesFound++;
				sentimentKeywords++;
			}
			if(negativeKeywords.containsKey(k))
			{
				negativesFound++;
				sentimentKeywords++;
			}
		}
		if (sentimentKeywords==0)
			return 0.0;
		return (double)(positivesFound-negativesFound)/(double)sentimentKeywords;
	}
	public double groupSentimentExtract(ArrayList<String> documents)
	{
		double sum=0.0;
		int size=0;
		for (String t:documents)
		{
			double sentiment=extractSentiment(t);
			if (sentiment!=0)
			{
				sum+=sentiment;
				size++;
			}
		}
		if (size==0)
		{
			return 0.0;
		}
		return sum/(double)size;
	}
	public double groupSentimentExtractTwitter(ArrayList<TwitterData> documents)
	{
		double sum=0.0;
		int size=0;
		for (TwitterData t:documents)
		{
			double sentiment=extractSentiment(t.getText());
			if (sentiment!=0)
			{
				sum+=sentiment;
				size++;
			}
		}
		if (size==0)
		{
			return 0.0;
		}
		return sum/(double)size;
	}
	public int groupSentimentNum(ArrayList<TwitterData> documents,int mode)
	{
		int sum=0;
		for (TwitterData t:documents)
		{
			double sentiment=extractSentiment(t.getText());
			if (mode==1)
			{
				if (sentiment>0)
				{
					sum++;
				}
			}
			if (mode==0)
			{
				if (sentiment<0)
				{
					sum++;
				}
			}
			if (mode==2)
			{
				if (sentiment==0)
				{
					sum++;
				}
			}
			
		}
		return sum;
	}
	public int groupSentimentNum(int mode)
	{
		return groupSentimentNum(this.documents,mode);
	}
	public double groupSentimentExtractTwitter()
	{
		return groupSentimentExtractTwitter(documents);
	}
	public void insert(TwitterData document)
	{
		documents.add(document);
	}
	public void clear()
	{
		documents=new ArrayList<TwitterData>();
	}
	public static void main(String[] args) throws IOException
	{
		SentimentAnalyzer sentimentAnalyzer=new SentimentAnalyzer();
		System.out.println(sentimentAnalyzer.extractSentiment("Τι έχουμε στην χώρα. χαζός."));
	}

}
