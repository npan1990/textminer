package gr.uoa.di.textminer.duplicate;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gr.uoa.di.textminer.corpus.Document;
import gr.uoa.di.textminer.corpus.TextCorpus;
import gr.uoa.di.textminer.corpus.TwitterCorpus;
import gr.uoa.di.textminer.hash.lsh.LSH;
import gr.uoa.di.textminer.servers.LSHServer;
import gr.uoa.di.textminer.vectorizers.CountVectorizer;
import gr.uoa.di.textminer.vectorizers.Vectorizer;

public class LSHDeduplicator {
	
	
	LSH lsh;
	HashMap<Integer,ArrayList<Document> > buckets;
	Vectorizer vectorizer;
	double threshold;
	boolean debug;
	Logger log;
	PrintWriter writer;
	
	
	public LSHDeduplicator(LSH lsh,Vectorizer vectorizer,double threshold) throws FileNotFoundException
	{
		this.lsh=lsh;
		buckets=new HashMap<Integer,ArrayList<Document>>();
		this.vectorizer=vectorizer;
		this.threshold=threshold;
		log=LoggerFactory.getLogger(LSHDeduplicator.class.getName());
		writer=new PrintWriter("duplicates.csv");
		
	}
	public void insert(String document)
	{
		HashMap<Integer,Double> vector=vectorizer.transform(document);
		List<Integer> fingerprints=lsh.getFingerprint(vector);
		for (int f:fingerprints)
		{
			if (!buckets.containsKey(f))
				buckets.put(f, new ArrayList<Document>());
			buckets.get(f).add(new Document(vector,document));
		}
	}
	public boolean isDuplicate(String document)
	{
		HashMap<Integer,Double> vector=vectorizer.transform(document);
		List<Integer> fingerprints=lsh.getFingerprint(vector);
		Document duplicateDocument=new Document(vector,document);
		boolean duplicate=false;
		for (int f:fingerprints)
		{
			if (!buckets.containsKey(f))
				continue;
			for (Document d:buckets.get(f))
			{
				if (d.similarity(duplicateDocument)>this.threshold)
				{
					if (debug)
					{
						writer.println(document+"|"+d.getDocument());
						writer.flush();
					}
					return true;
				}
			}
		}
		return false;
	}
	public void reset()
	{
		buckets=new HashMap<Integer,ArrayList<Document>>();
	}
	public LSH getLsh() {
		return lsh;
	}
	public void setLsh(LSH lsh) {
		this.lsh = lsh;
	}
	public HashMap<Integer, ArrayList<Document>> getBuckets() {
		return buckets;
	}
	public void setBuckets(HashMap<Integer, ArrayList<Document>> buckets) {
		this.buckets = buckets;
	}
	public Vectorizer getVectorizer() {
		return vectorizer;
	}
	public void setVectorizer(Vectorizer vectorizer) {
		this.vectorizer = vectorizer;
	}
	public double getThreshold() {
		return threshold;
	}
	public void setThreshold(double threshold) {
		this.threshold = threshold;
	}
	public boolean isDebug() {
		return debug;
	}
	public void setDebug(boolean debug) {
		this.debug = debug;
	}
	
	
	public static void main(String[] args) throws Exception
	{
		Double threshold=Double.valueOf(args[0]);
		Vectorizer vectorizer=new CountVectorizer(0,0,0,"cosine");
		vectorizer.fit(TwitterCorpus.getTwitterCorpus("livedrive_train.json"));
		int l=1;
		int k=4;
		int d=vectorizer.getSize();
		LSH lsh=new LSH(l,k,d);
		LSHDeduplicator duplicator=new LSHDeduplicator(lsh, vectorizer, threshold);
		ArrayList<String> corpus=TextCorpus.getTextCorpus("static.csv");
		duplicator.setDebug(true);
		int i=0;
		ArrayList<String> documents=new ArrayList<String>();
		int duplicates=0;
		for (String document:corpus)
		{
			if (!duplicator.isDuplicate(document))
			{
				duplicator.insert(document);
				documents.add(document);
			}
			else
			{
				duplicates++;
			}
		}
		/*System.out.println("Duplicates:"+duplicates);
		PrintWriter writer=new PrintWriter("deduplicate"+threshold+".csv");
		for(String pn:documents)
		{
			writer.println(pn);
		}
		writer.close();*/
	}
	

}
