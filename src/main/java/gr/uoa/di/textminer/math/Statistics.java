package gr.uoa.di.textminer.math;

import java.util.ArrayList;
import org.junit.Test;

public class Statistics<T> {
	
	
	double mean;
	double std; 
	
	
	public void calculateStatistics(ArrayList<T> values)
	{
		double sum=0.0;
		for(T t:values)
		{
			sum+=(int)(Integer)t;
		}
		mean=sum/(double)values.size();
		sum=0.0;
		for(T t:values)
		{
			sum+=((int)(Integer)t-mean)*((int)(Integer)t-mean);
		}
		std=Math.sqrt(sum/(double)values.size());
	}


	public double getMean() {
		return mean;
	}


	public void setMean(double mean) {
		this.mean = mean;
	}


	public double getStd() {
		return std;
	}


	public void setStd(double std) {
		this.std = std;
	}
	
	
	@Test
	public static void main(String[] args)
	{
		Statistics<Integer> statistics=new Statistics<Integer>();
		ArrayList<Integer> values=new ArrayList<Integer>();
		values.add(1);
		values.add(2);
		values.add(3);
		values.add(4);
		values.add(5);
		values.add(6);
		values.add(7);
		values.add(8);
		values.add(9);
		statistics.calculateStatistics(values);
		System.out.println("Mean:"+statistics.getMean());
		System.out.println("Std:"+statistics.getStd());
	}
	

}
