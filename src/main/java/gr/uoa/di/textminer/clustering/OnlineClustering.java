package gr.uoa.di.textminer.clustering;

import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gr.uoa.di.textminer.similarity.SimilarityUtils;
import gr.uoa.di.textminer.vectorizers.TfidfVectorizer;
import gr.uoa.di.textminer.vectorizers.Vectorizer;

public class OnlineClustering {
	

	double distanceThreshold;
	
	Vectorizer vectorizer;
	
	HashMap<Long,Cluster> clusters;
	
	Logger log;
	
	public static long aid;
	
	
	public OnlineClustering(double threshold,String vectorizerFilename)
	{
		distanceThreshold=threshold;
		vectorizer=TfidfVectorizer.load(vectorizerFilename);
		log=LoggerFactory.getLogger(OnlineClustering.class.getName());
		clusters=new HashMap<Long,Cluster>();
		aid=1;
	}
	
	
	public void insert(String document)
	{
		double minDistance=Double.MAX_VALUE;
		long minId=-1;
		HashMap<Integer,Double> documentVectorsArray=vectorizer.transform(document);
		for(long id:clusters.keySet())
		{
			Cluster c=clusters.get(id);
			HashMap<Integer,Double> centroid=c.getCentroid();
			double distance=1.0-SimilarityUtils.cosineSimilarity(centroid, documentVectorsArray);
			if (distance<minDistance)
			{
				minDistance=distance;
				minId=id;
			}
		}
		//System.out.println(minDistance);
		if(minDistance>this.distanceThreshold || minId==-1)
		{
			createCluster(document,documentVectorsArray);
		}
		else
		{
			clusters.get(minId).insert(document, documentVectorsArray);
		}
	}


	private void createCluster(String document,HashMap<Integer,Double> vector) {
		long id=aid;
		Cluster c=new Cluster(id,document,vector);
		clusters.put(id,c);
		aid+=1;
	}
	
	
	
	
	
	
	public HashMap<Long, Cluster> getClusters() {
		return clusters;
	}


	public void setClusters(HashMap<Long, Cluster> clusters) {
		this.clusters = clusters;
	}


	public static void main(String[] args)
	{
		OnlineClustering oc=new OnlineClustering(0.8,"vectorizer.json");
	}
	
	
	public void reset()
	{
		clusters=new HashMap<Long,Cluster>();
		aid=1;
	}
	
	
	
	

}
