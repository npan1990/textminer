package gr.uoa.di.textminer.clustering;

import java.util.ArrayList;
import java.util.HashMap;

public class Cluster {
	
	
	ArrayList<String> documents;
	HashMap<Integer,Double> centroid;
	int size;
	long id;
	
	
	
	public Cluster(long id2, String document, HashMap<Integer, Double> vector) {
		documents=new ArrayList<String>();
		centroid=vector;
		size=1;
		this.id=id2;
		documents.add(document);
	}
	public ArrayList<String> getDocuments() {
		return documents;
	}
	public void setDocuments(ArrayList<String> documents) {
		this.documents = documents;
	}
	public HashMap<Integer, Double> getCentroid() {
		return centroid;
	}
	public void setCentroid(HashMap<Integer, Double> centroid) {
		this.centroid = centroid;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public void insert(String document,HashMap<Integer,Double> dVectors)
	{
		documents.add(document);
		for (int t:dVectors.keySet())
		{
			if (centroid.containsKey(t))
			{
				centroid.put(t, centroid.get(t)*(double)size/(double)(size+1)+dVectors.get(t)*1.0/(size+1));
			}
			else
			{
				centroid.put(t, dVectors.get(t)*1.0/(size+1));
			}
		}
//		for (int t:centroid.keySet())
//		{
//			if(!dVectors.containsKey(t))
//			{
//				centroid.put(t, centroid.get(t)*(double)size/(double)(size+1));
//			}
//		}
		size=documents.size();
	}

}
