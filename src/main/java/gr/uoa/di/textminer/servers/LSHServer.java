package gr.uoa.di.textminer.servers;

import gr.uoa.di.textminer.corpus.TwitterCorpus;
import gr.uoa.di.textminer.duplicate.LSHDeduplicator;
import gr.uoa.di.textminer.hash.lsh.LSH;
import gr.uoa.di.textminer.vectorizers.CountVectorizer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LSHServer extends AbstractHandler {
	
	
	int l,k,d;
	HashMap<Integer,ArrayList<String>> buckets;
	CountVectorizer vectorizer;
	LSH lsh;
	Logger log;
	LSHDeduplicator duplicater;
	
	
	public LSHServer() throws Exception
	{
		super();
		buckets=new HashMap<Integer,ArrayList<String>>();
		vectorizer=new CountVectorizer(0,0,0,"cosine");
		vectorizer.fit(TwitterCorpus.getTwitterCorpus("livedrive_train.json"));
		l=1;
		k=4;
		d=vectorizer.getSize();
		lsh=new LSH(l,k,d);
		duplicater=new LSHDeduplicator(lsh, vectorizer, 0.9);
		log=LoggerFactory.getLogger(LSHServer.class.getName());
	}

	
	public static void main(String[] args) throws Exception
	{
        Server server = new Server(5001);
        server.setHandler(new LSHServer());
        server.start();
        server.join();
	}
	public void handle(String target,
            Request baseRequest,
            HttpServletRequest request,
            HttpServletResponse response) 
    throws IOException, ServletException
    {
		JSONObject responseJSON=new JSONObject();
		log.info(request.getParameter("action"));
		if (request.getParameter("action").equals("restart"))
		{
			this.duplicater.reset();
			responseJSON.put("action", "OK");
		}
		if (request.getParameter("action").equals("isDuplicate") && request.getParameter("text")!=null)
		{
			if (!duplicater.isDuplicate(request.getParameter("text")))
			{
				duplicater.insert(request.getParameter("text"));
				responseJSON.put("isDuplicate", "false");
			}
			else
			{
				responseJSON.put("isDuplicate", "true");
			}
		}
		response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_OK);
        baseRequest.setHandled(true);
        if (responseJSON!=null)
        {
        	response.getWriter().println(responseJSON.toString());
        }
        log.info(responseJSON.toString());
			
    }

}
