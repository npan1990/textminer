/**
 * 
 */
package gr.uoa.di.textminer.model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;

/**
 * @author nikos
 *
 */
public class TwitterData implements Serializable {

	long timestamp;
	public static String dataType="Twitter";
	String text=null;
	String user=null;
	double lon=0.0;
	double lat=0.0;
	boolean isAnomaly=false;
	double probabilityTraffic=0;
	double probabilityFlood=0;
	Date date;
	JSONObject json;
	HashMap<String,Double> geotagger=null;
	SimpleDateFormat sdf  = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");
	String id;
	public HashMap<String, Double> getGeotagger() {
		return geotagger;
	}
	public void setGeotagger(HashMap<String, Double> geotagger) {
		this.geotagger = geotagger;
	}
	public boolean isAnomaly() {
		return isAnomaly;
	}
	public void setAnomaly(boolean isAnomaly) {
		this.isAnomaly = isAnomaly;
	}
	public double getProbabilityTraffic() {
		return probabilityTraffic;
	}
	public void setProbabilityTraffic(double probability) {
		this.probabilityTraffic = probability;
	}
	public double getProbabilityFlood() {
		return probabilityFlood;
	}
	public void setProbabilityFlood(double probability) {
		this.probabilityFlood = probability;
	}
	
	public TwitterData(Long timestamp, String text,String user, Double lon,Double lat,Date date,JSONObject json) {
		this.timestamp=timestamp;
		this.text=text;
		this.user=user;
		this.lon=lon;
		this.lat=lat;
		this.date=date;
		this.json=json;
		
	}
	public TwitterData(String jsonS) throws ParseException
	{
		JSONObject jsonObject=(JSONObject) JSONValue.parse(jsonS);
		this.date=sdf.parse((String) jsonObject.get("created_at"));
		this.timestamp=this.date.getTime();
		JSONObject geo;
		Double lon,lat;
		try{
			geo=(JSONObject)jsonObject.get("geo");
			JSONArray coordinates=(JSONArray)geo.get("coordinates");
			lon=(Double) coordinates.get(0);
			lat=(Double) coordinates.get(1);
		}
		catch (Exception e)
		{
			lon=0.0;
			lat=0.0;
		}
		String text=(String) jsonObject.get("text");
		JSONObject user=(JSONObject)jsonObject.get("user");
		String user_screen_name=(String) user.get("screen_name");
		JSONObject json=new JSONObject(jsonObject);
		this.text=text;
		this.user=user_screen_name;
		this.lon=lon;
		this.lat=lat;
		this.json=jsonObject;
		this.id=(String) jsonObject.get("id_str");
	}
	public JSONObject getJson() {
		return json;
	}
	public void setJson(JSONObject json) {
		this.json = json;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "TwitterData [timestamp=" + timestamp + ", text=" + text
				+ ", user=" + user + ", lon=" + lon + ", lat=" + lat + "]";
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public double getLon() {
		return lon;
	}
	public void setLon(double lon) {
		this.lon = lon;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

}
