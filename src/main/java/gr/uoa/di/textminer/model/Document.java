package gr.uoa.di.textminer.model;

import java.util.Date;
import java.util.HashMap;

public class Document {
	
	
	String content;
	Date creationDate;
	long uid;
	String filename;
	static long aid=0;
	HashMap<Integer,Double> vector;
	HashMap<String,Double> documentVector;
	HashMap<Integer,Double> randomizedVector;
	String name;
	String friendlyName;
	public Document(String content, Date creationDate,String filename) {
		super();
		this.content = content;
		this.creationDate = creationDate;
		this.uid = aid;
		this.filename=filename;
		aid++;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public long getUid() {
		return uid;
	}
	public void setUid(long uid) {
		this.uid = uid;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public HashMap<Integer, Double> getVector() {
		return vector;
	}
	public void setVector(HashMap<Integer, Double> vector) {
		this.vector = vector;
	}
	public HashMap<String, Double> getDocumentVector() {
		return documentVector;
	}
	public void setDocumentVector(HashMap<String, Double> documentVector) {
		this.documentVector = documentVector;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFriendlyName() {
		return friendlyName;
	}
	public void setFriendlyName(String friendlyName) {
		this.friendlyName = friendlyName;
	}
	public HashMap<Integer, Double> getRandomizedVector() {
		return randomizedVector;
	}
	public void setRandomizedVector(HashMap<Integer, Double> randomizedVector) {
		this.randomizedVector = randomizedVector;
	}
	
}
