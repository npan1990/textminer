package gr.uoa.di.textminer.trends;

import gr.uoa.di.textminer.corpus.NGramExtractor;
import gr.uoa.di.textminer.entities.EntityExtractor;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HashtagTrendDetector extends TrendDetector {
	
	
	private static final Pattern TAG_PATTERN = 
			   Pattern.compile("(?:^|\\s|[\\p{Punct}&&[^/]])(#[\\p{L}0-9-_]+)");

	public HashtagTrendDetector(int N, int ngramsRange, double meanMin,
			int sizesMin,double threshold) throws FileNotFoundException, IOException {
		super(N, ngramsRange, meanMin, sizesMin,threshold);
		// TODO Auto-generated constructor stub
	}
	@Override
	public void insertDocument(String document)
	{
		Matcher mat = TAG_PATTERN.matcher(document);
		while (mat.find()) {
			  insert(mat.group(1));
			}
	}

}
