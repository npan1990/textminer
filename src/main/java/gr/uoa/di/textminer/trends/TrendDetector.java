package gr.uoa.di.textminer.trends;

import gr.uoa.di.textminer.common.CommonWords;
import gr.uoa.di.textminer.corpus.NGramExtractor;
import gr.uoa.di.textminer.datastructures.Counter;
import gr.uoa.di.textminer.math.Statistics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TrendDetector {
	
	
	public HashMap<String,ArrayList<Integer>> wordCounts;
	public int N;
	public HashMap<String,Double> mean;
	public HashMap<String,Double> std;
	int ngramsRange;
	Statistics<Integer> statistics;
	Counter<String> zscores;
	double meanThreshold;
	int sizeThreshold;
	CommonWords commonWords;
	double threshold;
	/**
	 * 
	 * @param N How many trends to return
	 * @param ngramsRange The range of the ngrams to use
	 * @param meanMin The minimum mean required
	 * @param sizesMin the minimum keyword length
	 * @param threshold The minimum zscore required
	 */
	public TrendDetector(int N,int ngramsRange,double meanMin,int sizesMin,double threshold) {
		super();
		wordCounts=new HashMap<String,ArrayList<Integer>>();
		mean=new HashMap<String,Double>();
		std=new HashMap<String,Double>();
		this.N=N;
		this.ngramsRange=ngramsRange;
		this.statistics=new Statistics<Integer>();
		this.meanThreshold=meanMin;
		this.sizeThreshold=sizesMin;
		this.threshold=threshold;
		try
		{
			commonWords=new CommonWords();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	public void insertDocument(String document)
	{
		for(int i=1;i<=this.ngramsRange;i++)
		{
			ArrayList<String> tokens=NGramExtractor.extractNgrams(document, i,sizeThreshold);
			for (String t:tokens)
			{
				insert(t);
			}
		}
	}
	public void insert(String ngramToken)
	{
		if(commonWords.isCommonWord(ngramToken))
			return ;
		if (!wordCounts.containsKey(ngramToken))
		{
			wordCounts.put(ngramToken, new ArrayList<Integer>());
			wordCounts.get(ngramToken).add(0);
		}
		int count=wordCounts.get(ngramToken).get(wordCounts.get(ngramToken).size()-1);
		count+=1;
		wordCounts.get(ngramToken).set(wordCounts.get(ngramToken).size()-1,count);
	}
	public void changeWindow()
	{
		calculateStatistics();
		for (String keyword:wordCounts.keySet())
		{
			wordCounts.get(keyword).add(0);
			if (wordCounts.get(keyword).size()>N)
			{
				wordCounts.get(keyword).remove(0);
			}
		}
	}
	public void calculateStatistics()
	{
		zscores=new Counter<>();
		for (String keyword:wordCounts.keySet())
		{
			if (wordCounts.get(keyword).size()==N)
			{
				statistics.calculateStatistics(new ArrayList<Integer>(wordCounts.get(keyword).subList(0,N-2)));
				mean.put(keyword, statistics.getMean());
				std.put(keyword, statistics.getStd());
				if(statistics.getStd()==0.0)
					std.put(keyword, 1.0);
				double zscore=(wordCounts.get(keyword).get(N-1)-mean.get(keyword))/std.get(keyword);
				if(statistics.getMean()<this.meanThreshold || keyword.length()<this.sizeThreshold || zscore<threshold)
					continue;
				zscores.add(keyword, zscore);
			}
		}
	}
	public List<String> calculateTrends(int n)
	{
		return zscores.mostCommon(n);
	}
	
	
	public static void main(String[] args)
	{
		TrendDetector t=new TrendDetector(10,1,0.0,0,1.0);
		for (int i=0;i<30;i++)
		{
			if( i>8)
			{
				t.insert("a");
				t.insert("a");
				t.insert("a");
				t.insert("a");
				t.insert("a");
				t.insert("a");
				t.insert("a");
				t.insert("b");
				t.insert("b");
				t.insert("b");
				t.insert("b");
				t.insert("b");
				t.insert("b");
				t.insert("b");
				t.insert("b");
				t.insert("b");
			}
			t.insert("a");
			t.insert("b");
			t.changeWindow();
			System.out.println(t.calculateTrends(5));
		}
		
	}
	
	

}
