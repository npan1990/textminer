package gr.uoa.di.textminer.trends;

import gr.uoa.di.textminer.corpus.NGramExtractor;
import gr.uoa.di.textminer.entities.EntityExtractor;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class EntityTrendDetector extends TrendDetector {
	
	
	EntityExtractor e;

	public EntityTrendDetector(int N, int ngramsRange, double meanMin,
			int sizesMin,double threshold) throws FileNotFoundException, IOException {
		super(N, ngramsRange, meanMin, sizesMin,threshold);
		// TODO Auto-generated constructor stub
		e=new EntityExtractor();
	}
	@Override
	public void insertDocument(String document)
	{
		ArrayList<String> tokens=e.extractEntities(document);
		for (String t:tokens)
		{
			t=t.toLowerCase();
			t=t.trim();
			insert(t);
		}
	}

}
