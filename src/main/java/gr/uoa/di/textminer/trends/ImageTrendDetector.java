package gr.uoa.di.textminer.trends;

import gr.uoa.di.textminer.corpus.NGramExtractor;
import gr.uoa.di.textminer.entities.EntityExtractor;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ImageTrendDetector extends TrendDetector {
	
	
	private static final Pattern TAG_PATTERN = 
			   Pattern.compile("(?:^|[\\W])((ht|f)tp(s?):\\/\\/|www\\.)"
                + "(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*"
                + "[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)");

	public ImageTrendDetector(int N, int ngramsRange, double meanMin,
			int sizesMin,double threshold) throws FileNotFoundException, IOException {
		super(N, ngramsRange, meanMin, sizesMin,threshold);
		// TODO Auto-generated constructor stub
	}
	@Override
	public void insertDocument(String document)
	{
		List<String> extractedUrls = extractUrls(document);

		for (String url : extractedUrls)
		{
		    insert(url);
		}
	}
	public static List<String> extractUrls(String text)
	{
	    List<String> containedUrls = new ArrayList<String>();
	    String urlRegex = "((https?|ftp|gopher|telnet|file):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
	    Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);
	    Matcher urlMatcher = pattern.matcher(text);

	    while (urlMatcher.find())
	    {
	        containedUrls.add(text.substring(urlMatcher.start(0),
	                urlMatcher.end(0)));
	    }

	    return containedUrls;
	}

}
