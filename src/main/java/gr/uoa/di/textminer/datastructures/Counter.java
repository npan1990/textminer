package gr.uoa.di.textminer.datastructures;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

public class Counter<T> {

    final ConcurrentMap<T, Double> counts = new ConcurrentHashMap<>();

    public void put(T it,double value) {
        add(it, value);
    }

    public void add(T it, double v) {
        counts.merge(it, v, Double::sum);
    }

    public List<T> mostCommon(int n) {
        return counts.entrySet().stream()
                // Sort by value.
                .sorted((e1, e2) -> Double.compare(e2.getValue(), e1.getValue()))
                // Top n.
                .limit(n)
                // Keys only.
                .map(e -> e.getKey())
                // As a list.
                .collect(Collectors.toList());
    }
    
    
    public static void main(String[] args) {
        Counter<String> c = new Counter<>();
        String[] numbers = {"Zero", "One", "Two", "Three", "Four", "Five", "Six"};
        for (int i = 0; i < numbers.length; i++) {
            c.add(numbers[i], (double)i);
        }
        System.out.println(c.mostCommon(3));
    }
}