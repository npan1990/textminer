package gr.uoa.di.textminer.hash;

import java.io.Serializable;
import java.util.HashMap;

import gr.uoa.di.textminer.bit.BitUtils;

public class SimHasher implements Serializable {
	int d;
	public int getD() {
		return d;
	}



	public void setD(int d) {
		this.d = d;
	}
	private UniversalHasher hasher;

	public SimHasher(int d) {
		super();
		this.d = d;
		hasher=new UniversalHasher(d);
	}
	
	
	
	public long hash(HashMap<String,Double> vector)
	{
		int[] v=new int[d];
		for (String s:vector.keySet())
		{
			long f=hasher.hash(s);
			for (int i=0;i<d;i++)
			{
				if(BitUtils.getBit(f, i+1)==1)
				{
					v[i]+=vector.get(s);
				}
				else
				{
					v[i]-=vector.get(s);
				}
			}
		}
		long n=0L;
		for (int i=0;i<d;i++)
		{
			if(v[i]>0)
			{
				n=BitUtils.setBit(n, i+1);
			}
			else
			{
				n=BitUtils.unsetBit(n, i+1);
			}
		}
		return n;
	}
	public long distance(long x1,long x2)
	{
		long n=x1 ^ x2 ;
		return Long.bitCount(n);
	}
}
