package gr.uoa.di.textminer.hash.lsh;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import com.mongodb.BasicDBList;

public class CosineFunction implements Serializable {
	
	/**
	 * @uml.property  name="vector"
	 * @uml.associationEnd  multiplicity="(0 -1)" elementType="java.lang.Float"
	 */
	//public List<Float> vector;
	public float[] vector;
	/**
	 * @uml.property  name="rn"
	 */
	Random rn;
	/**
	 * @uml.property  name="size"
	 */
	int size;
	
	
	public CosineFunction(int size, long seed)
	{
		this.size=size;
		vector=new float[size];
		rn=new Random(seed);
		for (int i=0;i<size;i++)
		{
			vector[i]=(float) rn.nextGaussian();
		}		
	}
	public CosineFunction(BasicDBList list)
	{
		this.size=list.size();
		vector=new float[size];
		for (int i=0;i<size;i++)
		{
			vector[i]=(float)(double)(Double)list.get(i);
		}		
	}
	
	public float getProjection(HashMap<Integer,Double> document)
	{
		float sum=0;
		for(Integer d:document.keySet())
		{
			sum+=1.0*vector[d];
			//System.out.println(vector.get(d));
		}
		return sum;
	}
	public float getProjection(int[] keys,double[] values)
	{
		float sum=0;
		for(int d:keys)
		{
			sum+=1.0*vector[d];
			//System.out.println(vector.get(d));
		}
		return sum;
	}
	public int getFingerprint(HashMap<Integer,Double> document)
	{
		if (getProjection(document)>0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	public int getFingerprint(int[] keys,double[] values)
	{
		if (getProjection(keys,values)>0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	public BasicDBList getMongoObject()
	{
		BasicDBList list=new BasicDBList();
		for (int i=0;i<size;i++)
		{
			list.add(vector[i]);
		}
		return list;
	}
	

}
