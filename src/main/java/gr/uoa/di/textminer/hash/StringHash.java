package gr.uoa.di.textminer.hash;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Hex;

public class StringHash {
	
	
	public static long simpleStringHash(String string)
	{
		long hash = 7;
		for (int i = 0; i < string.length(); i++) {
		    hash = hash*31 + string.charAt(i);
		}
		return hash;
	}
	public static String md5StringHash(String string) throws UnsupportedEncodingException, NoSuchAlgorithmException
	{
		byte[] bytesOfMessage = string.getBytes("UTF-8");
		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] thedigest = md.digest(bytesOfMessage);
		return new String(Hex.encodeHex(thedigest));
	}
	
	
	public static void main(String[] args) throws UnsupportedEncodingException, NoSuchAlgorithmException
	{
		System.out.println(StringHash.simpleStringHash("s sdf sdfsd fsdfsdf"));
		System.out.println(StringHash.md5StringHash("s sdf sdfsd fsdfsdf"));
	}

}
