package gr.uoa.di.textminer.hash.lsh;

import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;

public class CosineFamily implements Serializable {
	/**
	 * @uml.property  name="functions"
	 * @uml.associationEnd  multiplicity="(0 -1)" elementType="gr.uoa.di.kdd.twitter.model.CosineFunction"
	 */
	public List<CosineFunction> functions;
	/**
	 * @uml.property  name="k"
	 */
	int K;
	/**
	 * @uml.property  name="d"
	 */
	int D;
	
	
	public CosineFamily(int K,int D, long startSeed)
	{
		this.K=K;
		this.D=D;
		functions=new ArrayList<CosineFunction>();
		long seed = startSeed;
		for (int i=0;i<K;i++)
		{
			functions.add(new CosineFunction(D,seed));
			seed += 12345;
		}
	}
	public CosineFamily(BasicDBList list)
	{
		this.K=list.size();
		functions=new ArrayList<CosineFunction>();
		for (int i=0;i<K;i++)
		{
			CosineFunction cosineFunction=new CosineFunction((BasicDBList)list.get(i));
			functions.add(cosineFunction);
		}
		this.D=((BasicDBList)list.get(0)).size();
	}
	public int getFingerprint(HashMap<Integer,Double> document)
	{
		int fingerprint=0;
		int i=0;
		for(CosineFunction f:functions)
		{
			fingerprint+=f.getFingerprint(document)<<i;
			i++;
		}
		return fingerprint;
	}
	public int getFingerprint(int[] keys,double[] values)
	{
		int fingerprint=0;
		int i=0;
		for(CosineFunction f:functions)
		{
			fingerprint+=f.getFingerprint(keys,values)<<i;
			i++;
		}
		return fingerprint;
	}
	public BasicDBList getMongoObject()
	{
		BasicDBList list=new BasicDBList();
		for(CosineFunction f:functions)
		{
			list.add(f.getMongoObject());
		}
		return list;
	}
	public static void main(String[] args) throws FileNotFoundException
	{
		/*
		TwitterVectors twitterVectors=new TwitterVectors(2);
		System.out.println(twitterVectors.getDimension());
		CosineFamily lsh=new CosineFamily(2,twitterVectors.getDimension(), 10000);
		List<String> tweets=new LinkedList<String>();
		tweets.add("Justin bieber concert on March");
		tweets.add("Justin bieber I love you");
		tweets.add("Justin bieber playing tenis");
		tweets.add("my name is justin");
		tweets.add("Eurogroup meeting on Greece");
		tweets.add("Greece president Tsipras and Yiannis Varoufakis");
		tweets.add("the Dept of Greece is highly increased");
		tweets.add("Holidays in athens this summer");
		tweets.add("I have been to americ during summer");
		tweets.add("What About Greek Islands?");
		
		
		
		
		for (String t:tweets)
		{
			System.out.println(t+":"+lsh.getFingerprint(twitterVectors.toVector(t)));
		}
		*/
	}

}
