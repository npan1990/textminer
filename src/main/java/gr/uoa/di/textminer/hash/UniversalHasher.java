package gr.uoa.di.textminer.hash;

import java.io.Serializable;
import java.util.Random;
import java.util.UUID;

public class UniversalHasher implements Serializable {
	long p=2147483647;
	long a,b;
	long m;
	public UniversalHasher(long bits) {
		super();
		Random r=new Random();
		this.a = Math.abs(r.nextInt());
		this.b = Math.abs(r.nextInt());
		this.m = (long) Math.pow(2, bits);
	}
	public UniversalHasher(int a,int b,long bits) {
		super();
		Random r=new Random();
		this.a = a;
		this.b = b;
		this.m = (long) Math.pow(2, bits);
	}
	public long hash(int x)
	{
		return ((a*x+b)%p)%m;
	}
	public long hash(long x)
	{
		return ((a*x+b)%p)%m;
	}
	public long hash(String x)
	{
		long sum=0,i=1;
		for (char c:x.toCharArray())
		{
			sum=((sum*a)+(int)c)%p;
		}
		return this.hash(sum%p);
	}
	
	
	public static void main(String[] args)
	{
		String uuid = UUID.randomUUID().toString();
		UniversalHasher h=new UniversalHasher(64);
		String a="ab";
		String b="ba";
		System.out.println(h.hash(a));
		System.out.println(h.hash(b));
		
	}
	
	
	

}
