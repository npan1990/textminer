package gr.uoa.di.textminer.hash.lsh;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bson.types.ObjectId;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.Mongo;


public class LSH implements Serializable {
	/**
	 * @uml.property  name="families"
	 * @uml.associationEnd  multiplicity="(0 -1)" elementType="gr.uoa.di.kdd.twitter.model.CosineFamily"
	 */
	List<CosineFamily> families;
	/**
	 * @uml.property  name="l"
	 */
	public int L;
	/**
	 * @uml.property  name="k"
	 */
	public int K;
	/**
	 * @uml.property  name="d"
	 */
	public int D;
	Logger log;
	public LSH(int L,int K,int D)
	{
		log=LoggerFactory.getLogger(LSH.class.getName());
		families=new ArrayList<CosineFamily>();
		long seed = 10000;
		log.info("Generating hash functions");
		for (int i=0;i<L;i++)
		{
			families.add(new CosineFamily(K,D, seed));
			seed += 1000;
		}
		this.L=L;
		this.K=K;
		this.D=D;
		log.info("LSH Complete");
	}
	public LSH(int L,int K,int D,long seed)
	{
		log=LoggerFactory.getLogger(LSH.class.getName());
		families=new ArrayList<CosineFamily>();
		log.info("Generating hash functions");
		for (int i=0;i<L;i++)
		{
			families.add(new CosineFamily(K,D, seed));
			seed += 1000;
		}
		this.L=L;
		this.K=K;
		this.D=D;
		log.info("LSH Complete");
	}
	public LSH(BasicDBObject lshStoredObject)
	{
		log=LoggerFactory.getLogger(LSH.class.getName());
		families=new ArrayList<CosineFamily>();
		log.info("Loading hash families");
		BasicDBList storedFamiliesObject=(BasicDBList) lshStoredObject.get("families");
		for (int i=0;i<storedFamiliesObject.size();i++)
		{
			families.add(new CosineFamily((BasicDBList) storedFamiliesObject.get(i)));
		}
		this.L=lshStoredObject.getInt("L");
		this.K=lshStoredObject.getInt("K");
		this.D=lshStoredObject.getInt("D");
		log.info("LSH Complete");
	}
	public static LSH createLSHFromMongo(String mongoHost,int mongoPort,String mongoDatabase,String mongoCollection,String lshMongoName) throws UnknownHostException
	{
		Mongo mongo=new Mongo(mongoHost,mongoPort);
		DB db=mongo.getDB(mongoDatabase);
		DBCollection collection=db.getCollection(mongoCollection);
		BasicDBObject queryObject=new BasicDBObject();
		queryObject.put("lshMongoName", lshMongoName);
		BasicDBObject lshMongoObject=null;
		DBCursor cursor = collection.find(queryObject);
		while(cursor.hasNext())
		{
			lshMongoObject=(BasicDBObject) cursor.next();
		}
		return new LSH(lshMongoObject);
	}
	public List<Integer> getFingerprint(HashMap<Integer,Double> document)
	{
		List<Integer> fingerprints=new ArrayList<Integer>();
		for (int i=0;i<L;i++)
		{
			fingerprints.add(families.get(i).getFingerprint(document));
		}
		return fingerprints;
	}
	public List<Integer> getFingerprint(int[] keys,double values[])
	{
		List<Integer> fingerprints=new ArrayList<Integer>();
		for (int i=0;i<L;i++)
		{
			fingerprints.add(families.get(i).getFingerprint(keys,values));
		}
		return fingerprints;
	}
	public int getBucketSpace()
	{
		return 1<<K;
	}
	public void save(String name)
	{
		try{
			File f=new File("hash");f.mkdirs();
			FileOutputStream fout = new FileOutputStream("hash/"+name);
			ObjectOutputStream oos = new ObjectOutputStream(fout);   
			oos.writeObject(this);
			oos.close();
			log.info("Saved!");
	 
		   }catch(Exception ex){
			   ex.printStackTrace();
		   }
	}
	public static  LSH load(String name)
	{
		 try{
			 
			   FileInputStream fin = new FileInputStream("hash/"+name);
			   ObjectInputStream ois = new ObjectInputStream(fin);
			   LSH lsh = (LSH) ois.readObject();
			   ois.close();
			   return lsh;
	
		   }catch(Exception ex){
			   ex.printStackTrace();
			   return null;
		   } 
	}
	public BasicDBObject getMongoObject(String lshMongoName)
	{
		BasicDBObject object=new BasicDBObject();
		object.put("K", K);
		object.put("L", L);
		object.put("D", D);
		object.put("lshMongoName", lshMongoName);
		BasicDBList list=new BasicDBList();
		for(CosineFamily f:families)
		{
			list.add(f.getMongoObject());
		}
		object.put("families", list);
		return object;
	}
	public void storeToMongo(String mongoHost,int mongoPort,String mongoDatabase,String mongoCollection,String lshMongoName) throws UnknownHostException
	{
		Mongo mongo=new Mongo(mongoHost,mongoPort);
		DB db=mongo.getDB(mongoDatabase);
		DBCollection collection=db.getCollection(mongoCollection);
		BasicDBObject queryObject=new BasicDBObject();
		queryObject.put("lshMongoName", lshMongoName);
		collection.update(queryObject,getMongoObject(lshMongoName),true,false);
	}
	
	
	public static void main(String[] args) throws Exception
	{
		/*ArrayList<String> corpus=TwitterCorpus.getTwitterCorpus("small.json");
		CountVectorizer vectorizer=new CountVectorizer(0,0,0,"cosine");
		vectorizer.fit(corpus);
		LSH lsh=new LSH(2,4,vectorizer.getSize());
		for (String s:corpus)
		{
			System.out.println("Fingerprint:"+lsh.getFingerprint(vectorizer.transform(s)));
		}
		lsh.storeToMongo("localhost", 27017, "PlagiarismDetection", "LshN", "LSH_NAME");*/
		LSH lsh=LSH.createLSHFromMongo("localhost", 27017, "PlagiarismDetection", "LshN", "LSH_NAME");
		
	}

}
