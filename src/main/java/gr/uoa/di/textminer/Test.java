package gr.uoa.di.textminer;

import gr.uoa.di.textminer.hash.SimHasher;
import gr.uoa.di.textminer.vectorizers.TfidfVectorizer;
import gr.uoa.di.textminer.vectorizers.Vectorizer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONObject;

public class Test {
	
	
	public static void main(String[] args)
	{
		try{
			BufferedReader br = 
	                      new BufferedReader(new FileReader("small.json"));
	 
			String input;
			
			ArrayList<String> corpus=new ArrayList<String>();
			while((input=br.readLine())!=null){
				try
				{
					JSONObject tweet=new JSONObject(input);
					String textString=tweet.getString("text");
					if(textString==null)
						continue;
					corpus.add(textString);
				}
				catch(Exception e)
				{
					
				}
			}
			System.out.println("Size: "+corpus.size());
			Vectorizer vectorizer=new TfidfVectorizer(0,0,10,"simhash");
			vectorizer.fit(corpus);
			HashMap<Integer,Double> n=vectorizer.transform(corpus.get(10));
			for(int termInd:n.keySet())
			{
				System.out.println("Term: "+termInd+" "+n.get(termInd));
			}
			vectorizer.save("vectorizer.ser");
			vectorizer=TfidfVectorizer.load("vectorizer.ser");
			n=vectorizer.transform(corpus.get(0));
			for(int termInd:n.keySet())
			{
				System.out.println(corpus.get(10)+"Term: "+termInd+" "+n.get(termInd));
			}
			String s=corpus.get(0);
			vectorizer.getMostSimilarK("Κανεις δεν περασε.μονο κατι κορμια ειδα να κινουνται  ..", corpus, 3);
	 
		}catch(IOException io){
			io.printStackTrace();
		}	
	}

}
