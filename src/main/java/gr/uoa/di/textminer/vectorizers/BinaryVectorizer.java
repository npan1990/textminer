package gr.uoa.di.textminer.vectorizers;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class BinaryVectorizer extends Vectorizer {
	





	public BinaryVectorizer(int minTf, int minDf, int commonDf,String metric) {
		super(minTf, minDf, commonDf,metric);
		// TODO Auto-generated constructor stub
	}
	public BinaryVectorizer(int minTf,int minDf,int commonDf,String metric,HashMap<String,Integer> termsIndex,HashMap<Integer,String> termsMap,HashMap<Integer,Integer> termsDf,HashMap<Integer,Double> termsIdf,ArrayList<String> commonTerms)
	{
		super(minTf,minDf,commonDf,metric,termsIndex,termsMap,termsDf,termsIdf,commonTerms);
		
	}


	@Override
	public
	HashMap<Integer,Double> transform(String d) {
		String[] tokens=preprocess(d);
		HashMap<Integer,Double> countVector=new HashMap<Integer,Double>();
		for (String t:tokens)
		{
			int termInd=this.termsIndex.get(t)!=null?this.termsIndex.get(t):-1;
			if (termInd>=0)
			{
				if(countVector.containsKey(termInd))
					continue;
				else
					countVector.put(termInd, 1.0);
			}
		}
		return countVector;
	}
	public static BinaryVectorizer load(String name)
	{
		 try{
			 
			   FileInputStream fin = new FileInputStream("vectorizers/"+name);
			   ObjectInputStream ois = new ObjectInputStream(fin);
			   BinaryVectorizer binaryVectorizer = (BinaryVectorizer) ois.readObject();
			   ois.close();
	 
			   return binaryVectorizer;
	 
		   }catch(Exception ex){
			   ex.printStackTrace();
			   return null;
		   } 
	}


}
