package gr.uoa.di.textminer.vectorizers;

import java.text.Normalizer;
import java.util.HashMap;

public class TermExtractor {
	
	
	private HashMap<String,String> regexes;
	int minLength;

	public TermExtractor(int minLength)
	{
		regexes=new HashMap<String,String>();
		regexes.put("((https?|ftp|gopher|telnet|file|Unsure|http):((//)|(\\\\))+[\\w\\d:#@%/;$()~_\\+-=\\\\\\.&]*)", "");
		regexes.put("\\s+", " ");
		regexes.put("[^\\p{L}\\p{N}0-9 ]+", "");
		this.minLength=minLength;
	}
	public HashMap<String,Integer> getTerms(String document)
	{
		for (String regex:this.regexes.keySet())
		{
			document=document.replaceAll(regex,this.regexes.get(regex));
		}
		String terms[]=document.split(" ");
		HashMap<String,Integer> termWeights=new HashMap<String,Integer>();
		for (String t:terms)
		{
			t=t.toLowerCase();
			t=Normalizer.normalize(t, Normalizer.Form.NFD);
			t=t.replaceAll("\\p{M}", "");
			if (t.length()<minLength)
				continue;
			if (t.matches(".*\\d+.*"))
				continue;
			if (!termWeights.containsKey(t))
				termWeights.put(t, 0);
			termWeights.put(t, termWeights.get(t)+1);
		}
		return termWeights;
	}

}
