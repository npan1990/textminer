package gr.uoa.di.textminer.vectorizers;

import gr.uoa.di.textminer.corpus.TwitterCorpus;
import gr.uoa.di.textminer.hash.SimHasher;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.UnknownHostException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.Mongo;

public class CountVectorizer extends Vectorizer {
	





	/**
	 * 
	 */
	private static final long serialVersionUID = 7160270583836518806L;
	public CountVectorizer(int minTf, int minDf, int commonDf,String metric) {
		super(minTf, minDf, commonDf,metric);
		// TODO Auto-generated constructor stub
	}
	public CountVectorizer(int minTf,int minDf,int commonDf,String metric,HashMap<String,Integer> termsIndex,HashMap<Integer,String> termsMap,HashMap<Integer,Integer> termsDf,HashMap<Integer,Double> termsIdf,ArrayList<String> commonTerms)
	{
		super(minTf,minDf,commonDf,metric,termsIndex,termsMap,termsDf,termsIdf,commonTerms);
		
	}


	@Override
	public
	HashMap<Integer,Double> transform(String d) {
		String[] tokens=preprocess(d);
		HashMap<Integer,Double> countVector=new HashMap<Integer,Double>();
		for (String t:tokens)
		{
			t=t.toLowerCase();
			t=Normalizer.normalize(t, Normalizer.Form.NFD);
			t=t.replaceAll("\\p{M}", "");
			int termInd=this.termsIndex.get(t)!=null?this.termsIndex.get(t):-1;
			if (termInd>=0)
			{
				if(countVector.containsKey(termInd))
					countVector.put(termInd, countVector.get(termInd)+1);
				else
					countVector.put(termInd, 1.0);
			}
		}
		return countVector;
	}
	public static CountVectorizer load(String name)
	{
		 try{
			 
			   FileInputStream fin = new FileInputStream("vectorizers/"+name);
			   ObjectInputStream ois = new ObjectInputStream(fin);
			   CountVectorizer countVectorizer = (CountVectorizer) ois.readObject();
			   ois.close();
	 
			   return countVectorizer;
	 
		   }catch(Exception ex){
			   ex.printStackTrace();
			   return null;
		   } 
	}
	public static void main(String[] args) throws Exception
	{
		Vectorizer vectorizer=Vectorizer.loadFromMongo("DefaultVectorizer", "localhost", 27017, "PlagiarismDetection", "Vectorizers", "count");
		HashMap<Integer, Double> vectors = vectorizer.transform("hello my name is nick");
		System.out.println(vectors.size());
	}
	


}
