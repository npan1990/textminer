package gr.uoa.di.textminer.vectorizers;

import gr.uoa.di.textminer.datastructures.Counter;
import gr.uoa.di.textminer.hash.SimHasher;
import gr.uoa.di.textminer.model.Document;
import gr.uoa.di.textminer.similarity.SimilarityUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.UnknownHostException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;

public abstract class Vectorizer implements Serializable{
	HashMap<String,Integer> termsIndex;
	ArrayList<String> commonTerms;
	HashMap<Integer,String> termsMap;
	HashMap<String,String> regexes;
	HashMap<Integer,Integer> termsDf;
	HashMap<Integer,Double> termsIdf;
	Logger log;
	int minTf,minDf,commonDf;
	int documents;
	SimHasher similarityHasher;
	String metric;
	
	
	void fit(BufferedReader br) throws IOException {
		String line;
		while ((line=br.readLine())!=null)
		{
			documents++;
			processLine(line);
		}
		filter();
	}
	public void processLine(String line)
	{
		String[] terms=preprocess(line);
		for (String term:terms)
		{
			term=term.toLowerCase();
			term=Normalizer.normalize(term, Normalizer.Form.NFD);
			term=term.replaceAll("\\p{M}", "");
			if (term.matches(".*\\d+.*"))
				continue;
			if (this.termsIndex.containsKey(term))
			{
				int termInd=this.termsIndex.get(term);
				this.termsDf.put(termInd,this.termsDf.get(termInd)+1);
				continue;
			}
			this.termsIndex.put(term,this.termsIndex.size());
			this.termsDf.put(this.termsIndex.get(term), 1);
			this.termsMap.put(this.termsIndex.get(term), term);
		}
	}
	public void filter()
	{
		log.info("Processing Vectorizer");
		for (String term:this.termsIndex.keySet())
		{
			if (this.minDf>=0 && this.termsDf.get(this.termsIndex.get(term))<this.minDf)
			{
				this.termsMap.remove(termsIndex.get(term));
				this.termsIndex.remove(term);
			}
			if (this.commonDf>=0 && this.termsDf.get(this.termsIndex.get(term))>=this.commonDf)
			{
				this.commonTerms.add(term);
			}
		}
		log.info("Bulding the vectorizer Complete");
		log.info("Size:"+this.termsIndex.size());
	}
	public String[] preprocess(String line)
	{
		for (String regex:this.regexes.keySet())
		{
			line=line.replaceAll(regex,this.regexes.get(regex));
		}
		String terms[]=line.split(" ");
		return terms;
	}
	public void fit(ArrayList<String>  corpus)
	{
		
		for(String d:corpus)
		{
			documents++;
			processLine(d);
		}
		filter();
	}
	public abstract HashMap<Integer,Double> transform(String d);
	public Vectorizer(int minTf,int minDf,int commonDf,String metric)
	{
		this.minTf=minTf;
		this.minDf=minDf;
		this.commonDf=commonDf;
		documents=0;
		log=LoggerFactory.getLogger(this.getClass().getName());
		regexes=new HashMap<String,String>();
		regexes.put("((https?|ftp|gopher|telnet|file|Unsure|http):((//)|(\\\\))+[\\w\\d:#@%/;$()~_\\+-=\\\\\\.&]*)", "");
		regexes.put("\\s+", " ");
		regexes.put("[^\\p{L}\\p{N}0-9 ]+", "");
		this.termsIndex=new HashMap<String,Integer>();
		this.commonTerms=new ArrayList<String>();
		this.termsDf=new HashMap<Integer,Integer>();
		this.termsMap=new HashMap<Integer,String>();
		this.termsIdf=null;
		if(metric.equals("simhash"))
			this.similarityHasher=new SimHasher(64);
		this.metric=metric;
		
	}
	public Vectorizer(int minTf,int minDf,int commonDf,String metric,HashMap<String,Integer> termsIndex,HashMap<Integer,String> termsMap,HashMap<Integer,Integer> termsDf,HashMap<Integer,Double> termsIdf,ArrayList<String> commonTerms)
	{
		this.minTf=minTf;
		this.minDf=minDf;
		this.commonDf=commonDf;
		documents=0;
		log=LoggerFactory.getLogger(this.getClass().getName());
		regexes=new HashMap<String,String>();
		regexes.put("((https?|ftp|gopher|telnet|file|Unsure|http):((//)|(\\\\))+[\\w\\d:#@%/;$()~_\\+-=\\\\\\.&]*)", "");
		regexes.put("\\s+", " ");
		regexes.put("[^\\p{L}\\p{N}0-9 ]+", "");
		this.termsIndex=termsIndex;
		this.commonTerms=commonTerms;
		this.termsDf=termsDf;
		this.termsMap=termsMap;
		this.termsIdf=null;
		if(metric.equals("simhash"))
			this.similarityHasher=new SimHasher(64);
		this.metric=metric;
		
	}
	public void save(String name)
	{
		try{
			File f=new File("vectorizers");f.mkdirs();
			FileOutputStream fout = new FileOutputStream("vectorizers/"+name);
			ObjectOutputStream oos = new ObjectOutputStream(fout);   
			oos.writeObject(this);
			oos.close();
			log.info("Vectorizer Saved!");
		   }catch(Exception ex){
			   ex.printStackTrace();
		   }
	}
	public void saveToMongo(String vectorizerName,String mongoHost,int mongoPort,String mongoDatabase,String mongoCollection) throws UnknownHostException
	{
		log.info("Saving to mongo db");
		Mongo mongo=new Mongo(mongoHost,mongoPort);
		DB db = mongo.getDB(mongoDatabase);
		DBCollection collection = db.getCollection(mongoCollection);
		BasicDBObject vectorizerObject=new BasicDBObject();
		log.info("Connected to mongodb");
		vectorizerObject.put("vectorizerName", vectorizerName);
		BasicDBList terms=new BasicDBList();
		BasicDBList index=new BasicDBList();
		BasicDBList df=new BasicDBList();
		BasicDBList idf=new BasicDBList();
		BasicDBList commonTerms=new BasicDBList();
		for (String term:this.termsIndex.keySet())
		{
			terms.add(term);
			index.add(termsIndex.get(term));
			df.add(this.termsDf.get(termsIndex.get(term)));
			if(this.termsIdf!=null)
			{
				idf.add(this.termsIdf.get(termsIndex.get(term)));
			}
		}
		for(String term:this.commonTerms)
		{
			commonTerms.add(term);
		}
		vectorizerObject.put("minDf", this.minDf);
		vectorizerObject.put("minTf", this.minTf);
		vectorizerObject.put("commonDf", this.commonDf);
		vectorizerObject.put("metric", this.metric);
		vectorizerObject.put("terms", terms);
		vectorizerObject.put("index", index);
		vectorizerObject.put("df", df);
		if(this.termsIdf!=null)
		{
			vectorizerObject.put("idf",idf);
			vectorizerObject.put("type", "tfidf");
		}
		else
		{
			vectorizerObject.put("type", "count");
		}
		vectorizerObject.put("commonTerms", commonTerms);
		BasicDBObject queryObject=new BasicDBObject();
		queryObject.put("vectorizerName",vectorizerName);
		collection.update(queryObject,vectorizerObject,true,false);
		log.info("Complete");
	}
	public static Vectorizer loadFromMongo(String vectorizerName,String mongoHost,int mongoPort,String mongoDB,String mongoCollection,String type) throws UnknownHostException
	{
		Mongo mongo=new Mongo(mongoHost,mongoPort);
		DB db=mongo.getDB(mongoDB);
		DBCollection collection=db.getCollection(mongoCollection);
		BasicDBObject queryObject=new BasicDBObject();
		queryObject.put("vectorizerName", vectorizerName);
		DBCursor cursor = collection.find(queryObject);
		BasicDBObject vectorizerJson = null;
		while(cursor.hasNext())
		{
			vectorizerJson=(BasicDBObject) cursor.next();
			break;
		}
		int minTf=vectorizerJson.getInt("minTf");
		int minDf=vectorizerJson.getInt("minDf");
		int commonDf=vectorizerJson.getInt("commonDf");
		String metric=vectorizerJson.getString("metric");
		BasicDBList terms=(BasicDBList) vectorizerJson.get("terms");
		BasicDBList index=(BasicDBList) vectorizerJson.get("index");
		BasicDBList df=(BasicDBList) vectorizerJson.get("df");
		BasicDBList idf=(BasicDBList) vectorizerJson.get("idf");
		HashMap<String,Integer> termsIndex=new HashMap<String,Integer>();
		HashMap<Integer,String> termsMap=new HashMap<Integer,String>();
		HashMap<Integer,Integer> termsDf=new HashMap<Integer,Integer>();
		HashMap<Integer,Double> termsIdf=null;
		if(type.equals("tfidf"))
		{
			termsIdf=new HashMap<Integer,Double>();
		}
		ArrayList<String> commonTerms=new ArrayList<String>();
		for(int i=0;i<terms.size();i++)
		{
			termsIndex.put((String)terms.get(i), (int)index.get(i));
			termsMap.put((int)index.get(i), (String)terms.get(i));
			termsDf.put((int)index.get(i), (int)df.get(i));
			if (type.equals("tfidf"))
				termsIdf.put((int)index.get(i), (double)idf.get(i));
		}
		if(type.equals("count"))
			return new CountVectorizer(minTf, minDf, commonDf, metric, termsIndex, termsMap, termsDf, termsIdf, commonTerms);
		if(type.equals("binary"))
			return new BinaryVectorizer(minTf, minDf, commonDf, metric, termsIndex, termsMap, termsDf, termsIdf, commonTerms);
		if(type.equals("tfidf"))
			return new TfidfVectorizer(minTf, minDf, commonDf, metric, termsIndex, termsMap, termsDf, termsIdf, commonTerms);
		return null;
		
	}
	public HashMap<String,Double> getTermVector(HashMap<Integer,Double> vector)
	{
		HashMap<String,Double> termVector=new HashMap<String,Double>();
		for (int termInd:vector.keySet())
		{
			String term=termsMap.get(termInd);
			termVector.put(term, vector.get(termInd));
		}
		return termVector;
	}
	public String getMostSimilarK(String d,ArrayList<String> corpus,int K)
	{
		TreeMap<Double,ArrayList<String>> similarities=new TreeMap<Double,ArrayList<String>>(Collections.reverseOrder());;
		HashMap<Integer,Double> sourceVector=this.transform(d);
		for (String s:corpus)
		{
			HashMap<Integer,Double> tempsVector=this.transform(s);
			double similarity = 0;
			if(metric.equals("cosine"))
				similarity=SimilarityUtils.cosineSimilarity(sourceVector,tempsVector);
			if(metric.equals("simhash"))
			{
				HashMap<String,Double> terms1=this.getTermVector(sourceVector);
				HashMap<String,Double> terms2=this.getTermVector(tempsVector);
				similarity=SimilarityUtils.simhashSimilarity(terms1,terms2,similarityHasher);
			}
			if (!similarities.containsKey(similarity))
				similarities.put(similarity, new ArrayList<String>());
			similarities.get(similarity).add(s);
		}
		int i=0;
		for(Entry<Double, ArrayList<String>> entry : similarities.entrySet()) {
			  if(i>=K)
				  break;
			  i++;
			  double key = entry.getKey();
			  ArrayList<String> value = entry.getValue();
			  System.out.println("Printing at similarity  "+key);
			  for (String s:value)
			  {
				  System.out.println(s);
			  }
		}
		return "";
	}
	public HashMap<Document,Double> getMostSimilarDocuments(String document,ArrayList<Document> documents,double threshold)
	{
		HashMap<Integer,Double> sourceVector=this.transform(document);
		HashMap<Document,Double> similarDocuments=new HashMap<Document,Double>();
		for (Document s:documents)
		{
			double similarity=0.0;
			if(metric.equals("cosine"))
				similarity=SimilarityUtils.cosineSimilarity(sourceVector,s.getVector());
			if(metric.equals("simhash"))
			{
				HashMap<String,Double> terms1=this.getTermVector(sourceVector);
				HashMap<String,Double> terms2=this.getTermVector(s.getVector());
				similarity=SimilarityUtils.simhashSimilarity(terms1,terms2,similarityHasher);
			}
			if (similarity>=threshold)
				similarDocuments.put(s, similarity);
		}
		return similarDocuments;
		
	}
	public double calculateSimilarity(String document1,String document2)
	{
		HashMap<Integer,Double> vec1=this.transform(document1);
		HashMap<Integer,Double> vec2=this.transform(document2);
		double similarity=SimilarityUtils.cosineSimilarity(vec1,vec2);
		return similarity;
	}
	public double calculateSimilarity(HashMap<Integer,Double> vec1,HashMap<Integer,Double> vec2)
	{
		double similarity=SimilarityUtils.cosineSimilarity(vec1,vec2);
		return similarity;
	}
	public int getSize()
	{
		return this.termsIndex.size();
	}
}
