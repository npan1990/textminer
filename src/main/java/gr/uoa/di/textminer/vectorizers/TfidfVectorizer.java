package gr.uoa.di.textminer.vectorizers;

import gr.uoa.di.textminer.corpus.TwitterCorpus;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class TfidfVectorizer extends Vectorizer {
	
	





	public TfidfVectorizer(int minTf, int minDf, int commonDf,String metric) {
		super(minTf, minDf, commonDf,metric);
		this.termsIdf=new HashMap<Integer,Double>();
	}
	public TfidfVectorizer(int minTf,int minDf,int commonDf,String metric,HashMap<String,Integer> termsIndex,HashMap<Integer,String> termsMap,HashMap<Integer,Integer> termsDf,HashMap<Integer,Double> termsIdf,ArrayList<String> commonTerms)
	{
		super(minTf,minDf,commonDf,metric,termsIndex,termsMap,termsDf,termsIdf,commonTerms);
		this.termsIdf=termsIdf;
		
	}

	@Override
	public void fit(BufferedReader br) throws IOException
	{
		super.fit(br);
		for (String t:termsIndex.keySet())
		{
			int termInd=termsIndex.get(t);
			termsIdf.put(termInd, Math.log((double)documents/(double)termsDf.get(termInd)));
		}
	}
	@Override
	public void fit(ArrayList<String> corpus)
	{
		super.fit(corpus);
		for (String t:termsIndex.keySet())
		{
			int termInd=termsIndex.get(t);
			termsIdf.put(termInd, Math.log((double)documents/(double)termsDf.get(termInd)));
		}
	}

	@Override
	public
	HashMap<Integer,Double> transform(String d) {
		String[] tokens=preprocess(d);
		HashMap<Integer,Integer> countVector=new HashMap<Integer,Integer>();
		for (String t:tokens)
		{
			int termInd=this.termsIndex.get(t)!=null?this.termsIndex.get(t):-1;
			if (termInd>=0)
			{
				if(countVector.containsKey(termInd))
					countVector.put(termInd, countVector.get(termInd)+1);
				else
					countVector.put(termInd, 1);
			}
		}
		HashMap<Integer,Double> tfidfVector=new HashMap<Integer,Double>();
		for (int termInd:countVector.keySet())
		{
			tfidfVector.put(termInd, countVector.get(termInd)*termsIdf.get(termInd));
		}
		return tfidfVector;
	}
	public static TfidfVectorizer load(String name)
	{
		 try{
			 
			   FileInputStream fin = new FileInputStream("vectorizers/"+name);
			   ObjectInputStream ois = new ObjectInputStream(fin);
			   TfidfVectorizer tfidfVectorizer = (TfidfVectorizer) ois.readObject();
			   ois.close();
	 
			   return tfidfVectorizer;
	 
		   }catch(Exception ex){
			   ex.printStackTrace();
			   return null;
		   } 
	}
	
	
	public static void main(String[] args) throws Exception
	{
		Vectorizer vectorizer=new TfidfVectorizer(0, 0, 0, "cosine");
		vectorizer.fit(TwitterCorpus.getTwitterCorpus("sample.json"));
		vectorizer.save("greek.nikos");
	}
	


}
