package gr.uoa.di.textminer.corpus;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import org.json.JSONObject;

public class TwitterCorpus {
	
	
	public static ArrayList<String> getTwitterCorpus(String filename) throws Exception
	{
		ArrayList<String> corpus=new ArrayList<String>();
		BufferedReader br =new BufferedReader(new FileReader(filename));
		String input;
		while((input=br.readLine())!=null){
			try
			{
				JSONObject tweet=new JSONObject(input);
				String textString=tweet.getString("text");
				if(textString==null)
					continue;
				corpus.add(textString);
			}
			catch(Exception e)
			{
				continue;
			}
		}
		br.close();
		return corpus;
	}

}
