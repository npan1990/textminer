package gr.uoa.di.textminer.corpus;

import gr.uoa.di.textminer.duplicate.LSHDeduplicator;
import gr.uoa.di.textminer.hash.lsh.LSH;
import gr.uoa.di.textminer.vectorizers.CountVectorizer;
import gr.uoa.di.textminer.vectorizers.Vectorizer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.json.JSONObject;

public class TextCorpus {
	
	
	public static ArrayList<String> getTextCorpus(String filename) throws Exception
	{
		ArrayList<String> corpus=new ArrayList<String>();
		BufferedReader br =new BufferedReader(new FileReader(filename));
		String input;
		while((input=br.readLine())!=null){
			corpus.add(input);
		}
		br.close();
		return corpus;
	}
	public static ArrayList<String> getFileCorpus(List<String> filenames) throws Exception
	{
		ArrayList<String> corpus=new ArrayList<String>();
		for (String f:filenames)
		{
			BufferedReader br =new BufferedReader(new FileReader(f));
			String input;
			String c="";
			while((input=br.readLine())!=null){
				c+=input;
			}
			br.close();
			corpus.add(c);
		}
		return corpus;
	}
	
	
	
	
	public static ArrayList<String> getFileCorpus(String directory,String format) throws Exception
	{
		ArrayList<String> corpus=new ArrayList<String>();
		File folder = new File(directory);
		File[] listOfFiles = folder.listFiles();

		for (int i = 0; i < listOfFiles.length; i++) {
		  File file = listOfFiles[i];
		  if (file.isFile() && file.getName().endsWith(format)) {
		    String content = FileUtils.readFileToString(file);
		    corpus.add(content);
		  } 
		}
		return corpus;
	}
	
	
	public static void main(String[] args) throws Exception
	{
		Vectorizer vectorizer=CountVectorizer.load("vectorizer.j");
		int l=1;
		int k=4;
		int d=vectorizer.getSize();
		LSH lsh=LSH.load("lsh.j");
		LSHDeduplicator duplicator=new LSHDeduplicator(lsh, vectorizer, 0.7);
		for (String n:TextCorpus.getFileCorpus("/home/nikos/get", ".txt"))
		{
			duplicator.insert(n);
		}
		File file=new File("4_Αναλυτική Περιγραφή Open eClass 2.10.pdf.txt");
		String n=FileUtils.readFileToString(file);
		if (duplicator.isDuplicate(n))
			System.out.println("Is Duplicate");
	}

}