package gr.uoa.di.textminer.corpus;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SentenceExtractor {
		
	HashMap<String,String> regexes;
	int minSize;
	Logger log;
	
	
	public SentenceExtractor(int minSize)
	{
		log=LoggerFactory.getLogger(SentenceExtractor.class.getName());
		regexes=new HashMap<String,String>();
		regexes.put("(\n\r)", " ");
		regexes.put("((https?|ftp|gopher|telnet|file|Unsure|http):((//)|(\\\\))+[\\w\\d:#@%/;$()~_\\+-=\\\\\\.&]*)", "");
		regexes.put("\\s+", " ");
		regexes.put("[^\\p{L}\\p{N}0-9 \\.]+", "");
		regexes.put("\\p{M}", "");
		this.minSize=minSize;
	}
	
	
	public ArrayList<String> getSentences(String document)
	{
		document=Normalizer.normalize(document, Normalizer.Form.NFD);
		for (String r:regexes.keySet())
		{
			document=document.replaceAll(r, regexes.get(r));
		}
		String[] sentencesStrings=document.split("\\.");
		ArrayList<String> sentences=new ArrayList<String>();
		for(String s:sentencesStrings)
		{
			String[] tokens=s.split(" ");
			if (tokens.length>=this.minSize)
			{
				sentences.add(s);
			}
		}
		return sentences;
	}
	public ArrayList<String> getSentencesNgram(String document,int n)
	{
		document=Normalizer.normalize(document, Normalizer.Form.NFD);
		for (String r:regexes.keySet())
		{
			document=document.replaceAll(r, regexes.get(r));
		}
		String[] sentencesStrings=document.split("\\.");
		ArrayList<String> ngrams=new ArrayList<String>();
		for(String s:sentencesStrings)
		{
			String[] tokens=s.split(" ");
			if (tokens.length>=this.minSize)
			{
				ngrams.addAll(NGramExtractor.extractNgrams(s, n,0));
			}
		}
		return ngrams;
	}
	public static void main(String[] args)
	{
		SentenceExtractor se=new SentenceExtractor(2);
		System.out.println(se.getSentencesNgram("Αυτό είναι μια δοκιμή. \nΜιά πρόταση πολυ μεγάλη.          Μία μεγαλύτερη πρόταση. Μια πρόταση με αρκετόυς αριθμούς.",4));
	}
	
	

}
