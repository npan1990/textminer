package gr.uoa.di.textminer.corpus;

import java.util.ArrayList;
import java.util.HashMap;

public class NGramExtractor {
	
	

	public static ArrayList<String> extractNgrams(String document,int n,int minSize)
	{
		HashMap<String, String> regexes;
		ArrayList<String> ngrams=new ArrayList<String>();
		regexes=new HashMap<String,String>();
		regexes.put("((https?|ftp|gopher|telnet|file|Unsure|http):((//)|(\\\\))+[\\w\\d:#@%/;$()~_\\+-=\\\\\\.&]*)", "");
		regexes.put("\\s+", " ");
		regexes.put("[^\\p{L}\\p{N}0-9 ]+", "");
		for (String regex:regexes.keySet())
		{
			document=document.replaceAll(regex,regexes.get(regex));
		}
		document=document.trim();
		String terms[]=document.split(" ");
		for (int i=0;i<terms.length;i++)
		{
			if (terms[i].length()<minSize)
				continue;
			String ngram=terms[i];
			if ((i+n)>terms.length)
				break;
			int counters=1;
			for(int j=i+1;j<i+n;j++)
			{
				if (terms[j].length()<minSize)
					continue;
				ngram+=" "+terms[j];
				counters++;
			}
			if (counters>=n)
				ngrams.add(ngram);
		}
		return ngrams;
	}
	
	public static void main(String[] args)
	{
		System.out.println(NGramExtractor.extractNgrams("This is a test test test nikos", 4,0));
	}

}
