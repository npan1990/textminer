package gr.uoa.di.textminer.corpus;

import gr.uoa.di.textminer.similarity.SimilarityUtils;

import java.util.HashMap;

public class Document {
	
	
	private HashMap<Integer,Double> vector;
	private String document;
	public Document(HashMap<Integer, Double> vector, String document) {
		super();
		this.vector = vector;
		this.document = document;
	}
	public HashMap<Integer, Double> getVector() {
		return vector;
	}
	public void setVector(HashMap<Integer, Double> vector) {
		this.vector = vector;
	}
	public String getDocument() {
		return document;
	}
	public void setDocument(String document) {
		this.document = document;
	}
	public double similarity(Document d)
	{
		return SimilarityUtils.cosineSimilarity(this.vector, d.getVector());
	}

}
